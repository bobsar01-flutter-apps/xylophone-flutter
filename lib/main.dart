import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  void playSound(int id) {
    final player = AudioCache();
    player.play('note$id.wav');
  }

  Expanded buildKey({int id, Color color}) {
    return Expanded(
      child: FlatButton(
        onPressed: () {
          playSound(id);
        },
        color: color,
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.black,
        body: SafeArea(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              buildKey(color: Colors.red, id: 1),
              buildKey(color: Colors.orange, id: 2),
              buildKey(color: Colors.yellow, id: 3),
              buildKey(color: Colors.green, id: 4),
              buildKey(color: Colors.blue, id: 5),
              buildKey(color: Colors.indigo, id: 6),
              buildKey(color: Colors.purple, id: 7),
              buildKey(color: Colors.redAccent, id: 8),
            ],
          ),
        ),
      ),
    );
  }
}
